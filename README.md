Version Info
============

Show Tiny Tiny RSS (and related, e.g. PHP and OS) version info via Shift+V.

A [Tiny Tiny RSS](https://tt-rss.org) plugin.

Installation
------------
1. Clone the repo to **version_info** in your tt-rss **plugins.local** directory:

   `git clone https://gitlab.tt-rss.org/wn/ttrss-version-info.git version_info`

2. Enable the plugin @ Preferences / Plugins
